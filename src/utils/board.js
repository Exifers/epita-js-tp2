const horizontalAlignment = (board, boardWidth = 3, boardHeight = 3, length = 3, symbol = 'X') => {
    board.map(symbol => symbol || '-'); // replacing empty cells by '-' so that they're still there after the join

    for (let i = 0; i < boardHeight; i++) {
        const ret = board.slice(i * boardWidth, (i + 1) * boardWidth).join('').includes(symbol.repeat(length));
        if (ret) {
            return symbol;
        }
    }

    return false;
};

const verticalAlignment = (board, boardWidth = 3, boardHeight = 3, length = 3, symbol = 'X') => {
    board.map(symbol => symbol || '-');

    for (let i = 0; i < boardWidth; i++) {
        const ret = board.filter((e, index) => (index - i) % boardWidth === 0).join('').includes(symbol.repeat(length));
        if (ret) {
            return symbol;
        }
    }

    return false;
};

const diagonalBottomLeftToTopRightAlignment = (board, boardWidth = 3, boardHeight = 3, length = 3, symbol = 'X') => {
    board.map(symbol => symbol || '-');

    for (let i = 0; i < Math.max(boardWidth, boardHeight); i++) {
        const ret = board.filter((e, index) => {
            const x = index % boardWidth;
            const y = Math.floor(index / boardHeight);
            return x + y === i;
        }).join('').includes(symbol.repeat(length));
        if (ret) {
            return symbol;
        }
    }

    return false;
};

const diagonalTopLeftToBottomRightAlignment = (board, boardWidth = 3, boardHeight = 3, length = 3, symbol = 'X') => {
    board.map(symbol => symbol || '-');

    for (let i = 0; i < Math.max(boardWidth, boardHeight); i++) {
        const ret = board.filter((e, index) => {
            const x = index % boardWidth;
            const y = Math.floor(index / boardHeight);
            return (boardWidth - 1 - x) + y === i;
        }).join('').includes(symbol.repeat(length));
        if (ret) {
            return symbol;
        }
    }

    return false;
};

export const alignment = (board, boardWidth = 3, boardHeight = 3, length = 3, symbols = ['X', 'O']) => {
    const functions = [
        horizontalAlignment,
        verticalAlignment,
        diagonalBottomLeftToTopRightAlignment,
        diagonalTopLeftToBottomRightAlignment
    ];
    for (let i = 0; i < functions.length; i++) {
        for (let s = 0; s < symbols.length; s++) {
            const ret = functions[i](board, boardWidth, boardHeight, length, symbols[s]);
            if (ret) {
                return ret;
            }
        }
    }
    return '';
};