import React, {Component} from "react";
import {css} from "glamor";

const info = css({
    fontFamily: 'sans-serif'
});

class GameInfo extends Component {
    render() {
        if (this.props.currentPlayer) {
            return (
                <h3 className={`${info} mt-4`}>It's your turn {this.props.currentPlayer.name}</h3>
            );
        }
        else if (this.props.winner) {
            return (
                <h3 className={`${info} mt-4`}>{this.props.winner.name} wins !</h3>
            );
        }
        else {
            return (
                <h3 className={`${info} mt-4`}>Tie !</h3>
            );
        }
    }
}

export default GameInfo;
