import React, {Component} from "react";

class RestartButton extends Component {
    render() {
        if (this.props.restart) {
            return (
                <button type={'button'} className={'btn btn-primary my-3'} onClick={this.props.onClick}>RESTART</button>
            );
        }
        return null;
    }
}

RestartButton.defaultProps = {
    onClick: () => {}
};

export default RestartButton;