import React, {Component} from "react";
import {css} from "glamor";

const cell = css({
    backgroundColor: '#b5d0dd',
});

// We could have used the :hover property from glamor but the instructions told to use onMouseOver/onMouseOut
const cellHovered = css({
    backgroundColor: '#96b2ce'
});

const cellStyle = {
    display: "block",
    width: "200px",
    height: "200px",
    border: "1px solid #333",
    outline: "none",
    textAlign: "center",
    lineHeight: "200px",
    cursor: "pointer",
    color: "#485e8b",
    fontFamily: "sans-serif",
    fontSize: "180px"
};

class Cell extends Component {
    constructor(props) {
        super(props);

        this.state = {
            mouseOver: false
        };

        this.handleMouseOver = this.handleMouseOver.bind(this);
        this.handleMouseOut = this.handleMouseOut.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    handleMouseOver(event) {
        this.setState({
            mouseOver: true
        });
    }

    handleMouseOut(event) {
        this.setState({
            mouseOver: false
        });
    }

    handleClick(event) {
        this.props.onClick({...event, position: this.props.position});
    }

    render() {
        return (
            <div style={cellStyle} onMouseOver={this.handleMouseOver} onMouseOut={this.handleMouseOut} onClick={this.handleClick}
                 className={`${cell}` + (this.state.mouseOver ? ` ${cellHovered}` : '')}>{this.props.value || ''}</div>
        );
    }
}

Cell.defaultProps = {
    onClick: () => {}
};

export default Cell;
