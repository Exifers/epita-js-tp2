import React from "react";
import Board from "../components/Board";
import GameInfo from "../components/GameInfo";
import {alignment} from "../utils/board";
import RestartButton from "../components/RestartButton";

const gameLayoutStyle = {
  width: 650,
  height: `calc(90%)`,
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  margin: "auto",
  textAlign: 'center'
};

const players = [
  {
    name: 'player 1',
    symbol: 'X'
  },
  {
    name: 'player 2',
    symbol: 'O'
  }
];

class GameLayout extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      cells: Array(9).fill(null),
      currentPlayer: players[0],
      winner: null
    };

    this.handleCellClick = this.handleCellClick.bind(this);
    this.handleRestart = this.handleRestart.bind(this);
  }

  // getDerivedStateFromProps is called before every render,
  // use it to infer new state values from props or state changes.
  static getDerivedStateFromProps(props, state) {
    if (alignment(state.cells)) {
      state.winner = state.currentPlayer === players[0] ? players[1] : players[0];
      state.currentPlayer = null;
    }
    else if (state.cells.filter(e => e).length === 9) {
      state.winner = null;
      state.currentPlayer = null;
    }
    return state;
  }

  handleCellClick(event) {
    const position = event.position;
    if (this.state.winner) { return; }
    if (this.state.cells[position]) { return; }

    this.setState(state => ({
      cells: (state.cells[position] = state.currentPlayer.symbol) && state.cells,
      currentPlayer: state.currentPlayer === players[0] ? players[1] : players[0]
    }));
  }

  handleRestart(event) {
    this.setState({
      cells: Array(9).fill(null),
      currentPlayer: players[0],
      winner: null
    });
  }

  render() {
    return (
      <div
        style={gameLayoutStyle}
      >
        <GameInfo currentPlayer={this.state.currentPlayer} winner={this.state.winner}/>
        <Board cells={this.state.cells} onCellClick={this.handleCellClick} />
        <RestartButton restart={!this.state.currentPlayer} onClick={this.handleRestart}/>
      </div>
    );
  }
}

export default GameLayout;
